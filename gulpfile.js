// Import
var gulp = require("gulp"),
  sass = require("gulp-sass"),
  postcss = require("gulp-postcss"),
  autoprefixer = require("autoprefixer"),
  cssnano = require("cssnano"),
  sourcemaps = require("gulp-sourcemaps"),
  browserSync = require("browser-sync").create(),
  imagemin = require('gulp-imagemin')

// Put this after including our dependencies
var paths = {
  styles: {
    // By using styles/**/*.sass we're telling gulp to check all folders for any sass file
    src: "scss/**/*.scss",
    // Compiled files will end up in whichever folder it's found in (partials are not compiled)
    dest: "css"
  }

  // Easily add additional paths
  // ,html: {
  //  src: '...',
  //  dest: '...'
  // }
};

function style() {
  return (
    gulp
    .src(paths.styles.src)
    // Initialize sourcemaps before compilation starts
    .pipe(sourcemaps.init())
    .pipe(sass({ outputStyle: 'expanded' }))
    .on("error", sass.logError)
    // Use postcss with autoprefixer and compress the compiled file using cssnano
    // .pipe(postcss([autoprefixer(), cssnano()]))
    // Now add/write the sourcemaps
    // .pipe(sourcemaps.write())
    .pipe(gulp.dest(paths.styles.dest))
    .pipe(browserSync.stream())
  );
}

function images() {
  return gulp
    .src(paths.images.src)
    .pipe(imagemin())
    .pipe(gulp.dest(paths.imgs.dest));
}

function reload() {
  browserSync.reload();
}

function watch() {
  browserSync.init({
    server: {
      baseDir: ["./", "./templates"]
    }
  });

  gulp.watch(paths.styles.src, style);
  // gulp.watch(paths.images.src, images);
  // gulp.watch(paths.images.src, ['images']);
  // We should tell gulp which files to watch to trigger the reload
  // This can be html or whatever you're using to develop your website
  // Note -- you can obviously add the path to the Paths object
  gulp.watch("templates/*.html").on('change', reload);
}


// ...
exports.style = style
exports.watch = watch